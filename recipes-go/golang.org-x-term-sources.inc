SRC_URI += "https://proxy.golang.org/golang.org/x/term/@v/v0.0.0-20220411215600-e5f449aeb171.zip;srcoutput=golang.org/x/term;srcinput=golang.org/x/term@v0.0.0-20220411215600-e5f449aeb171;downloadfilename=golang-org-x-term-0.0.0-20220411215600-e5f449aeb171.zip;name=golang-org-x-term"
SRC_URI[golang-org-x-term.sha256sum] = "337357d223826a26b61a563a842f351d5e4df26add227c310fa863d4f2e569d7"

GOSRC_LICENSE += "\
    BSD-3-Clause \
"
LIC_FILES_CHKSUM += "\
    file://src/golang.org/x/term/LICENSE;md5=5d4950ecb7b26d2c5e4e7b4e0dd74707 \
"

GOSRC_INCLUDEGUARD += "golang.org-x-term-sources.inc"

require ${@bb.utils.contains('GOSRC_INCLUDEGUARD', 'golang.org-x-sys-sources.inc', '', 'golang.org-x-sys-sources.inc', d)}
